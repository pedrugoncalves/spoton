<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\TrainingController;


class userController extends Controller
{
    public function newUser()
        {
            return view ('create_user');
        }

    public function createUser(Request $request)
        {
            $data = $request->all();
            $user = User::where('email', $data['email'])->first();
        
            if($user == null)
                {
                    $data['password'] = bcrypt($data['password']);
                    User::create($data);
                    return redirect()->route('new.user');
                }
                else
                    {
                        $error_password = ['Cliente já existe'];
                        return redirect(route('new.user', compact('error_password')));

                    }
            return $data;
        }

    public function listUsers()
        {
            $users = User::All();
            return view ('list_users', compact('users'));
        }


    public function viewUser(Request $request)
        {
            $user = User::where('id_user', $request->id_user)->first();
            return view ('view_user', compact('user'))
        }

    public function newMetter()
            {
                return view ('create_metter');
            }
       
     
            
    public function createMetter(Request $request)
            {
                return $request;
            }


    public function SigIn()
        {
            return view ('auth.login');
        }
    
    public function Login(Request $request)
        {
        $dados = $request->all();
        

        $login = $dados['email'];
        $password = $dados['password'];

        $user = User::where('email', $login)->first();

        if(Auth::check() || ($user && Hash::check($password, $user->password))){
            Auth::login($user);
            return redirect(route('dashboard'));
           
        } else {$erros_bd = ['Essa conta não existe!'];
            return redirect(route('login', compact('erros_bd')));
        }

       return  $login . '  '.$password;
        }
}   
