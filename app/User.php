<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'date_birth', 'c_c', 'nif', 'profission', 'telephone',
        'mobile_phone', 'address', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function trainings()
        {
            return $this->belongsToMany(training::class, 'participates', 'id_user', 'id_trainings');

        }

    public function monthlypayments()
        {
            return $this->belongsToMany(monthlypayment::class, 'payment', 'id_user', 'id_montlypayments');
        }

    public function metters()
        {
            return $this->hasMany(metter::class, 'id_user', 'id_user');
        }
}
