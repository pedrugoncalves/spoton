<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class metter extends Model
{
    protected $table = 'metters';

    protected $fillable = [
        'weight', 'm_g', 'm_m', 'i_m_c', 'd_c_i', 'bio_age', 'visceral_fat', 'h_2_0_c', 'm_o', 'date'
    ];

    public function users()
        {
            return $this->belongsTo(User::class, 'id_user', 'id_user');
        }
}
