<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class monthlypayment extends Model
{
    protected $table = 'monthlypayments';

    protected $fillable = [
        'type_monthlypayments', 'month_monthlypayments', 'year_monthlypayments', 'safe_monthlypayments'
    ];

    public function users()
        {
            return $this->belongsToMany(user::class, 'payments', 'id_monthlypayments', 'id_user', );

        }
}
