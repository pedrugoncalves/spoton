<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class training extends Model
{
    protected $table = 'trainings';

    protected $fillable = [
        'date_trainings', 
    ];

    public function users()
        {
            return $this->belongsToMany(user::class, 'participates', 'id_trainings', 'id_user', );

        }
    
    public function trainingphases()
        {
            return $this->hasMany(trainingphase::class, 'id_trainings', 'id_trainings');
        }
}
