<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trainingphase extends Model
{
    protected $table = 'trainingphases';

    protected $fillable = [
        'title', 'description', 'id_trainings' 
    ];

    public function trainings()
        {
            return $this->belongsTo(training::class, 'id_trainings', 'id_trainings');
        }
}

