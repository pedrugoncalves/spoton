<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
          //$table->string('type');
            $table->string('name');
            $table->date('date_birth');
            $table->string('c_c');
            $table->integer('nif');
            $table->string('profission');
            $table->integer('telephone');
            $table->integer('mobile_phone');
            $table->string('address');
            $table->rememberToken();
            $table->timestamps();
            
        });
        DB::statement("ALTER TABLE users AUTO_INCREMENT = 1000;");

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
