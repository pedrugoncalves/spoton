<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metters', function (Blueprint $table) {
            $table->increments('id_metters');
            $table->float('weight');
            $table->float('m_g');
            $table->float('m_m');
            $table->float('i_m_c');
            $table->float('d_c_i');
            $table->float('bio_age');
            $table->float('visceral_fat');
            $table->float('h_2_o_c');
            $table->float('m_o');
            $table->date('date');
            $table->integer('id_user')->unsigned();
            $table->timestamps();


        });

        Schema::table('metters', function($table)
            {
                $table->foreign('id_user')->references('id_user')->on('users')
                      ->onDelete('cascade');
            }
        );



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metters');
    }
}
