<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthlypaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthlypayments', function (Blueprint $table) {
            $table->increments('id_monthlypayments');
            $table->integer('type_monthlypayments');
            $table->integer('month_monthlypayments');
            $table->year('year_monthlypayments');
            $table->boolean('safe_monthlypayments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthlypayments');
    }
}
