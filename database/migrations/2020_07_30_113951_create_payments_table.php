<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id_payments');
            $table->float('value_payments');
            $table->integer('id_user')->unsigned();
            $table->integer('id_monthlypayments')->unsigned();
            $table->timestamps();
        });

        Schema::table('payments', function($table)
            {
                $table->foreign('id_user')->references('id_user')->on('users')
                      ->onDelete('cascade');
                $table->foreign('id_monthlypayments')->references('id_monthlypayments')
                      ->on('monthlypayments')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
