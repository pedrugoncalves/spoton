<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participates', function (Blueprint $table) {
            $table->increments('id_participates');
            $table->text('grades');
            $table->boolean('presence');
            $table->integer('id_user')->unsigned();
            $table->integer('id_trainings')->unsigned();
            $table->timestamps();
        });

        Schema::table('participates', function($table)
            {
                $table->foreign('id_user')->references('id_user')->on('users')
                      ->onDelete('cascade');
                $table->foreign('id_trainings')->references('id_trainings')->on('trainings')
                      ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participates');
    }
}
