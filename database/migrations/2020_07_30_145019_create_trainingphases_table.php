<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingphasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingphases', function (Blueprint $table) {
            $table->increments('id_tainingphases');
            $table->string('title');
            $table->text('description');
            $table->integer('id_trainings')->unsigned();
            $table->timestamps();
        });

        Schema::table('trainingphases', function($table)
            {
                $table->foreign('id_trainings')->references('id_trainings')
                      ->on('trainings')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingphases');
    }
}
