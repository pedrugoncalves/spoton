@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   

            

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                                   
                            <div class="card-title mb-3">Criar Metter</div>
                            
                            <form method="POST" action="{{ route('create.metter') }}">
                                    @csrf
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="weight">Peso</label>
                                        <input type="text" class="form-control form-control-rounded" id="weight" placeholder="peso" name="weight">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="m_g">Massa Gorda</label>
                                        <input type="text" class="form-control form-control-rounded" id="m_g"  placeholder="massa gorda" name="m_g">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="m_m">Massa Magra</label>
                                        <input class="form-control form-control-rounded" id="m_m" placeholder="massa magra" name="m_m">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="i_m_c">Indice de Massa Corporal</label>
                                        <input class="form-control form-control-rounded" id="mobile_phone" placeholder="indice de massa Corporal" name="i_m_c">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="d_c_i">Ingestão Calorica Diaria</label>
                                        <input class="form-control form-control-rounded" id="address" placeholder="ingestão calorica diaria" name="d_c_i">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="bio_age">Idade Biologica</label>
                                        <input type="text" class="form-control form-control-rounded" id="bio_age" placeholder="idade biologica" name="bio_age">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="visceral_fat">Gordura Visceral</label>
                                        <input type="text" class="form-control form-control-rounded" id="visceral_fat" placeholder="gordura visceral" name="visceral_fat">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="h_2_o_c">Percentagem de água no corpo</label>
                                        <input type="text" class="form-control form-control-rounded" id="h_2_o_c" placeholder="Percentagem de água no corpo" name="h_2_o_c">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="m_o">Massa Óssea</label>
                                        <input type="text" class="form-control form-control-rounded" id="m_o" placeholder="Percentagem de água no corpo" name="m_o">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="date">Data</label>
                                        <input type="date" class="form-control form-control-rounded" id="date" placeholder="data" name="date">
                                    </div>

                                    <div class="col-md-12">
                                         <button class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
