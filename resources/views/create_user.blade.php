@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   

            

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                                   
                            <div class="card-title mb-3">Criar Cliente</div>
                            
                            <form method="POST" action="{{ route('create.user') }}">
                                    @csrf
                                    
                                    @if (isset($error_password))
                                    <div class="alert alert-danger">
                                    @foreach ($error_password as $error)
                                    <p> {{ $error }} </p>
                                    @endforeach
                                    </div>
                                    @endif
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="name">Nome</label>
                                        <input type="text" class="form-control form-control-rounded" id="name" placeholder="Nome" name="name">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control form-control-rounded" id="email"  placeholder="email" name="email">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control form-control-rounded" id="password" placeholder="password" name="password">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="repeatpassword">Password</label>
                                        <input type="password" class="form-control form-control-rounded" id="repeatpassword" placeholder="password" name="repeatpassword">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="telephone">Telefone</label>
                                        <input class="form-control form-control-rounded" id="telephone" placeholder="telefone" name="telephone">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="mobilephone">Telemóvel</label>
                                        <input class="form-control form-control-rounded" id="mobile_phone" placeholder="telemóvel" name="mobile_phone">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="address">Morada</label>
                                        <input class="form-control form-control-rounded" id="address" placeholder="morada" name="address">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="datebirth">Data de Nascimento</label>
                                        <div class="input-group">
                                            <input id="datebirth" class="form-control form-control-rounded" placeholder="aaaa-mm-dd" name="date_birth" >
                                        </div>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cc">Cartão de Cidadão</label>
                                        <input type="text" class="form-control form-control-rounded" id="c_c" placeholder="cartão de cidadão" name="c_c">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="nif">NIF</label>
                                        <input type="text" class="form-control form-control-rounded" id="nif" placeholder="nif" name="nif">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="profission">Profissão</label>
                                        <input type="text" class="form-control form-control-rounded" id="profission" placeholder="profissão" name="profission">
                                    </div>

                                    

                                    <!--<div class="col-md-6 form-group mb-3">
                                        <label for="picker1">Select</label>
                                        <select class="form-control form-control-rounded">
                                            <option>Option 1</option>
                                            <option>Option 1</option>
                                            <option>Option 1</option>
                                        </select>
                                    </div>-->

                                    <div class="col-md-12">
                                         <button class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script>
        var password = document.getElementById("password")
            , repeatpassword = document.getElementById("repeatpassword");

        function validatePassword(){
            if(password.value != repeatpassword.value) {
                repeatpassword.setCustomValidity("Passwords não corresponde");
                } else {
                    repeatpassword.setCustomValidity('');
                }
            }
            password.onchange = validatePassword;
            repeatpassword.onkeyup = validatePassword;
</script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
