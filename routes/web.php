<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('starter');
});



Route::get('/sigIn', 'userController@SigIn')->name('sigIn');
Route::POST('/login', 'userController@Login')->name('login');


Route::get('/home', 'trainingController@home')->name('home');


Route::get('/new_user', 'userController@newUser')->name('new.user');
Route::POST('/create_user', 'userController@createUser')->name('create.user');
Route::get('/list_users', 'userController@listUsers')->name('list.users');
Route::get('/view_user', 'userController@viewUser')->name('view.user');


Route::get('/new_metter', 'userController@newMetter')->name('new.metter');
Route::POST('/create_metter', 'userController@createMetter')->name('create.metter');


